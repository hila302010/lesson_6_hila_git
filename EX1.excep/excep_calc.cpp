#include <iostream>
#include <string>
using std::cout;
using std::endl;

std::string error(void)
{
return "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year\n";
}

int add(int a, int b) {
if (a == 8200 || b == 8200) {
throw error();
}
return a + b;
}

int  multiply(int a, int b) {
int sum = 0;
for(int i = 0; i < b; i++) {
sum = add(sum, a);
};
if (sum == 8200) {
throw error();
}
return sum;
}

int  pow(int a, int b)
{
int exponent = 1;
for (int i = 0; i < b; i++)
{
exponent = multiply(exponent, a);
}
if (exponent == 8200) {
throw error();
}
return exponent;
}

int main(void)
{
// print 5 ^ 5 = 3125
try {
cout << pow(8200, 5) << endl;
}
catch (std::string  special) {
std::cout << special;
}
getchar();
}