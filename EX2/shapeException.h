#pragma once
#include <exception>

class shapeException : public std::exception
{
public:
	virtual const char* what() const
	{
		return "This is a shape exception!\n";
	}
};

class InputExeption : public std::exception
{
public:
	virtual const char* what() const
	{
		// get rid of failure state
		std::cin.clear();
		// discard 'bad' character(s)
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		return "ERROR -- You did not enter an integer \n";
	}
};