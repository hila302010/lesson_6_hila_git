#include <iostream>
#include <string>
using std::cout;
using std::endl;
#define ERROR -1
std::string error(void)
{
	return "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year\n";
}

int add(int a, int b)
{
	if (a + b == 8200) {
		return ERROR;
	}
	return a + b;
}

int  multiply(int a, int b)
{
	int sum = 0;
	for (int i = 0; i < b; i++) {
		sum = add(sum, a);
	};
	if (sum == 8200) {
		return ERROR;
	}
	return sum;
}

int  pow(int a, int b)
{
	int exponent = 1;
	for (int i = 0; i < b; i++) {
		exponent = multiply(exponent, a);
	};
	if (exponent == 8200) {
		return ERROR;
	}
	return exponent;
}

int main(void)
{
	// print 5 ^ 5 = 3125
	int sln = pow(8200, 1);
	if (sln == ERROR) {
		std::cout << error();
	}
	else {
		cout << sln;
	}
	getchar();
}